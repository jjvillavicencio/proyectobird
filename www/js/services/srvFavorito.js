starter.service('srvFavorito',function($http) {

    this.servicioFavorito = function(item, userId, estado) {
      link = 'http://data.utpl.edu.ec/wsmovil/social/crearfavorito';
      return $http.post( link,
        {
          "sujeto"  : item,
          "usuario" : userId,
          "estado"  : estado
        }
      )
    };
    this.listaFavoritos = function(user) {
      return $http.get('http://data.utpl.edu.ec/biows/social/favorite?usuario='+user);
    };
});
