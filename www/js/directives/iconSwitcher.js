starter.directive('iconSwitcher', function() {

  return {
    restrict : 'A',

    link : function(scope, elem, attrs) {

      var currentState = true;

      elem.on('click', function() {
        if(currentState === true) {
          angular.element(elem).removeClass(attrs.onIcon);
          angular.element(elem).addClass(attrs.offIcon);
        } else {
          angular.element(elem).removeClass(attrs.offIcon);
          angular.element(elem).addClass(attrs.onIcon);
        }
        currentState = !currentState
      });
    }
  };
});
