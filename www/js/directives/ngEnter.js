//Directiva para ejecutar accion al presionar tecla enter
starter.directive('ngenter', function () {
    return function (scope, element, attrs) {
        element.bind("keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngenter);
                    scope.fasJson = {
                      'principal' : '',
                      '0' : '',
                      '1' : '',
                      '2' : '',
                      '3' : '',
                      '4' : ''
                    }
                });

                event.preventDefault();
            }
        });
    };
});
