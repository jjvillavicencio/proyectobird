// Ionic Starter App

var starter = angular.module('starter', ['ionic', 'starter.services', 'ngStorage', 'ngCordova','ngCordovaOauth', 'ionRate']);

starter.run(function($ionicPlatform, $cordovaFile, $localStorage, $cordovaSQLite, $rootScope, $ionicPopup, $cordovaToast, srvFavorito, $ionicHistory, srvDetalle) {
  //------Variable global-----
  $rootScope.refrescar = {
    'Servicio'  : null,
    'Refrescar' : null
  };
  //------------FIN-----------
  $rootScope.ConexionInter = function () {
    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        $ionicPopup.confirm({
          title: "Sin conexión a Internet",
          content: "Su dispositivo no tiene acceso a internet, ¿Desea salir?"
        })
        .then(function(result) {
          if(result) {
            ionic.Platform.exitApp();
          }
        });
      }
    }
  }
  $ionicPlatform.ready(function() {
    //ABRIR BASE DE DATOS
    db = $cordovaSQLite.openDB( {
      name: "UTPL.birds", iosDatabaseLocation: 'default'
    });



    // Ocultar barra de accesorios del teclado
    // Deshabilitar barra de scroll del teclado
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if (window.StatusBar) {
      // org.apache.cordova.required
      StatusBar.styleDefault();
    }
    // ----------------LEER SI EXISTE BASE DE DATOS ------
    $rootScope.getUserDB = function () {
      $rootScope.userId = null;
      try {
        var query = "SELECT * FROM usuario";

        $cordovaSQLite.execute(db, query)
        .then(function(res) {
          console.log('acui');
          if(res.rows.length > 0) {
            $rootScope.userDB = res.rows.item(0);
            $rootScope.userId = res.rows.item(0).id;
          } else {
            $rootScope.userDB = null;
          }
        }, function (err) {
          $rootScope.userDB = null;
          console.log('Linea 63, app.js, eeror query: ' + err);
        });
      } catch (e) {
        console.log("Error verificaciòn BD: "+e );
      }
    }
    $rootScope.getUserDB();

    // -----------------------FIN---------------------------

    $rootScope.agregarFavoritos = function() {
      // CREAR BASE DE DATOS SQLITE
      db = $cordovaSQLite.openDB( {
        name: "UTPL.birds",
        iosDatabaseLocation: 'default'
      });
      //OBTENER LISTA DE FAVORITOS
      srvFavorito.listaFavoritos($localStorage.usuarioID)
      .success(function (listFav) {
        console.log(listFav);
        $cordovaSQLite.execute( db, "DROP TABLE IF EXISTS favoritos" );
        for (var fav in listFav) {
          console.log(listFav[fav].estado);
          if (listFav[fav].estado) {
            // CREAR TABLA FAVORITOS EN SQLite
            $cordovaSQLite.execute( db, "CREATE TABLE IF NOT EXISTS favoritos (idUsuario integer, thumb text, description text, id text, type text, title text, estado text)" );
            var query = "INSERT INTO favoritos (idUsuario, thumb, description, id, type, title, estado) VALUES (?,?,?,?,?,?,?)";
            // INSERTAR DATOS DEL ITEM FAVORITO EN BD INTERNA
            try {
              thumb = listFav[fav].image.thumb;

            } catch (e) {
              thumb = 'img/default_avatar2.png';
            }
            $cordovaSQLite.execute( db, query, [$localStorage.usuarioID, thumb, listFav[fav].description, listFav[fav].id,  listFav[fav].type, listFav[fav].title, listFav[fav].estado] )
            .then(function(insertFav) {
              console.log("Then "+insertFav);
            }, function (err) {
              console.error("Error al insertar datos SQLite: "+err);
              alert("Error al insertar datos SQLite: "+err);
            });
          }
        }
      })
      .error(function (data) {
        console.log(data);
      })
      .finally(function () {

      });
    }

    //AGREGAR ITEM A FAVORITO
    $rootScope.addFav = function (item, refresh) {
      //VERIFICAR SI ESTA REGISTRADO
      if ($localStorage.usuarioID != null) {
        //AGREGAR O QUITAR DE FAVORITOS
        estado = !item.estado;
        //LLAMAR AL SERVICIO PARA AGREGAR FAVORITO
        console.log('it:');
        console.log(item);
        console.log(item.id);
        srvFavorito.servicioFavorito(item.id, $localStorage.usuarioID, estado)
        //SI POST SE EJECUTA CORRECTAMENTE
        .success(function(data){
          $rootScope.agregarFavoritos();
          if (estado) {
            item.favorito = 'estrellaOn';
            item.estado = true;
            $cordovaToast.show('Favorito marcado', 'long', 'bottom');
            console.log("aqui: "+refresh);
            if (refresh) {
              $rootScope.refrescar.Refrescar = true;
            }else{
              $rootScope.refrescar.Refrescar = false;
            }
            console.log($rootScope.refrescar);
            $rootScope.getUserDB();
          }else{
            item.favorito = '';
            item.estado = false;
            $cordovaToast.show('Favorito desmarcado', 'long', 'bottom');
            console.log("aqui: "+refresh);
            if (refresh) {
              $rootScope.refrescar.Refrescar = true;
            }else{
              $rootScope.refrescar.Refrescar = false;
            }
            console.log($rootScope.refrescar);
          }
        })
        //SI HAY UN ERRO EN EL POST
        .error(function(data){
          alert("Error "+ JSON.stringify(data));
        })
        //AL FINALIZAR EL SERVICIO
        .finally(function($ionicLoading) {

        });
        // SI NO ESTA REGISTRADO
      }else{
        //POP UP DE ERROR
        // var alertPopup=$ionicPopup.alert({
        //   title: 'Sin acceso',
        //   subTitle:'Necesitas registrate para usar esta opción.',
        //   okType:'default: button-assertive'
        // });
        $cordovaToast.show('Necesitas registrate', 'long', 'center')

      }
    }

    //OBTENER CLASE PARA LA ESTRELLA DE FAVORITO
    $rootScope.claseFav = function (estado) {
      if (estado == 'estrellaOn') {
        return 'estrellaOn';
      }else {
        return '';
      }
    }

    $rootScope.agregarRanks = function() {
      //OBTENER LISTA DE FAVORITOS
      srvDetalle.listaRank($localStorage.usuarioID)
      .success(function (listRank) {
        console.log(listRank);
        $cordovaSQLite.execute( db, "DROP TABLE IF EXISTS rankeo" );
        // CREAR TABLA RANKEO EN SQLite
        $cordovaSQLite.execute( db, "CREATE TABLE IF NOT EXISTS rankeo (idUsuario integer, description text, id text, type text, title text, estado text, rank integer)" );
        for (var rank in listRank) {
          console.log(listRank[rank].rank);
          if (listRank[rank].rank > 0) {
            var query = "INSERT INTO rankeo (idUsuario, description, id, type, title, estado, rank) VALUES (?,?,?,?,?,?,?)";
            // INSERTAR DATOS DEL ITEM RANKEADO EN BD INTERNA
            $cordovaSQLite.execute( db, query, [$localStorage.usuarioID, listRank[rank].description, listRank[rank].id,  listRank[rank].type, listRank[rank].title, listRank[rank].estado, listRank[rank].rank] )
            .then(function(insertRank) {
              console.log("Then "+insertRank);
            }, function (err) {
              console.error("Error al insertar datos SQLite: "+err);
              alert("Error al insertar datos SQLite: "+err);
            });
          }
        }
      })
      .error(function (data) {
        console.log(data);
      })
      .finally(function () {

      });
    }

    //---------------COMPARTIR PUBLICACIÓN-------------
      $rootScope.compartir = function (red,recurso) {
        switch (red) {
          case 'facebook':
            if (recurso === undefined) {
              urlss = null;
            }else{
              urlss = 'https://www.facebook.com/sharer/sharer.php?u='+recurso;
            }
            window.open(urlss||'https://www.facebook.com/sharer/sharer.php?u=http%3A//smartland.utpl.edu.ec/', '_system', 'location=no'); return false;

            break;
          case 'twitter':
            if (recurso === undefined) {
              urlss = null;
            }else{
              urlss = 'https://twitter.com/intent/tweet?url='+recurso;
            }
            window.open(urlss||'https://twitter.com/intent/tweet?url=http://smartland.utpl.edu.ec/', '_system', 'location=no'); return false;

            break;
          case 'google':
            if (recurso === undefined) {
              urlss = null;
            }else{
              urlss = 'https://plus.google.com/share?text=Compartir&url='+recurso;
            }
            window.open(urlss||'https://plus.google.com/share?text=Compartir&url=http%3A//smartland.utpl.edu.ec/', '_system', 'location=no'); return false;

            break;

        }
      }
    //----------------------FIN------------------------
  });
});

starter.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.navBar.alignTitle('center');
  $stateProvider
  // State abstracto con tabs de la aplicación
  .state('tabs', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    cache : true
  })
  // Vistas para el Satate Tab Buscar
  //Vista para realizar busquedas
  .state('tabs.buscar',{
    url:'/buscar',
    views:{
      'tab-buscar':{
        templateUrl: 'templates/tab-buscar.html',
        controller:'busquedaCtrl'
      }
    }
  })
  //Vista para mostrar detalles de un item
  .state('tabs.detalle',{
    url:'/detalle?id',
    views:{
      'tab-buscar':{
        templateUrl: 'templates/detalle.html',
        controller:'detalleCtr'
      }
    }
  })
  //Vista para mostrar multimedia del item
  .state('tabs.detalle-galeria',{
    url:'/detalle-galeria?id&tipo',
    views:{
      'tab-buscar':{
        templateUrl: 'templates/detalle-galeria.html',
        controller:'multimediaCtrl'
      }
    }
  })
  //Vistas para el State Tab Favoritos
  .state('tabs.favoritos',{
    url:'/favoritos',
    views:{
      'tab-favoritos':{
        templateUrl:'templates/tab-favoritos.html',
        controller : 'favoritosCtrl'
      }
    }
  })


  //Vista para mostrar detalles de un item
  .state('tabs.favoritosDet',{
    url:'/detalle?id&tab',
    views:{
      'tab-favoritos':{
        templateUrl: 'templates/detalle.html',
        controller:'detalleCtr'
      }
    }
  })

  //Vista para mostrar multimedia del item
  .state('tabs.detalle-galeria-fav',{
    url:'/detalle-galeria?id&tipo',
    views:{
      'tab-favoritos':{
        templateUrl: 'templates/detalle-galeria.html',
        controller:'multimediaCtrl'
      }
    }
  })
  //Vistas para el State Tab Usuario
  .state('tabs.usuario',{
    url:'/usuario',
    views:{
      'tab-usuario':{
        templateUrl:'templates/tab-usuario.html',
        controller : 'loginCtrl'
      }
    }
  })
  // Estado por defecto de la aplicación
  $urlRouterProvider.otherwise('/tab/buscar');
});
