starter.controller("detalleCtr",function($scope,$state,$localStorage, srvDetalle,$ionicPopup,$stateParams,$ionicModal,$ionicLoading,ReportSvc,$http,$rootScope, $cordovaSQLite){
  $scope.$on('$ionicView.beforeEnter', function(){
    $rootScope.ConexionInter();
    //VERIFICAR SI ESTA REGISTRADO
    if ($localStorage.usuarioID != null) {
      var query = "SELECT * FROM rankeo WHERE idUsuario = ? AND id = ?";
      $cordovaSQLite.execute(db, query, [$localStorage.usuarioID, $stateParams.id]).then(function(res) {
        if(res.rows.length > 0) {
          $scope.results=[];
          for(var i=0; i<res.rows.length; i++){
            $scope.results.push(res.rows.item(i));
          }
          console.log('Este res:');
          console.log($scope.results[0]);
          $scope.rate = $scope.results[0].rank;
          console.log('rango caras:'+$scope.rate);
        } else {
          console.log('no esta rankeado');
          $scope.rate = 0;
        }
      }, function (err) {
        alert(JSON.stringify(err));
      });
    }
  });


  //Modulo de PDF
  $scope.runReport = _runReport;
  $scope.clearReport = _clearReport;
  _activate();

  function _activate() {

    $scope.$on('ReportSvc::Progress', function(event, msg) {
      _showLoading(msg);
    });
    $scope.$on('ReportSvc::Done', function(event, err) {
      _hideLoading();
    });
  }

  function _runReport() {
    //if no cordova, then running in browser and need to use dataURL and iframe
    if (!window.cordova) {
      ReportSvc.runReportDataURL( {},{} )
      .then(function(dataURL) {
        //set the iframe source to the dataURL created
        document.getElementById('pdfImage').src = dataURL;
      });
      return true;
    }
    //if codrova, then running in device/emulator and able to save file and open w/ InAppBrowser
    else {
      ReportSvc.runReportAsync( {},{} )
      .then(function(filePath) {
        //log the file location for debugging and oopen with inappbrowser
        console.log('report run on device using File plugin');
        console.log('ReportCtrl: Opening PDF File (' + filePath + ')');
        window.open(filePath, '_blank', 'location=no,closebuttoncaption=Close,enableViewportScale=yes');
        _hideLoading();
      });
      return true;
    }
  }

  //reset the iframe to show the empty html page from app start
  function _clearReport() {
    document.getElementById('pdfImage').src = "empty.html";
  }
  //
  // Loading UI Functions: utility functions to show/hide loading UI
  //
  function _showLoading(msg) {
    $ionicLoading.show({
      template: msg
    });
  }


  $scope.lugar = true;

  function _hideLoading(){
    $ionicLoading.hide();
  }


  $scope.show = function() {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner>'
    });
  };

  $scope.hide = function(){
    $ionicLoading.hide();
  };


  $scope.irMultimedia= function(idRecibe,tipMult){
    console.log(idRecibe);
    if ($stateParams.tab === 'fav') {
      $state.go('tabs.detalle-galeria-fav',{id:idRecibe,tipo:tipMult});
    }else{
      $state.go('tabs.detalle-galeria',{id:idRecibe,tipo:tipMult});
    }
  }


  $scope.show();
  srvDetalle.servicioDetalle($stateParams.id)
  .success(function(data){
    $scope.res=data;
    if ($localStorage.usuarioID != null) {
      var query = "SELECT * FROM favoritos WHERE id = ?";
      $cordovaSQLite.execute(db, query, [$scope.res.id]).then(function(res) {
        if(res.rows.length > 0) {
          $scope.res.favorito = 'estrellaOn';
          $scope.res.estado = true;
          console.log('uno');
        } else {
          $scope.res.favorito = '';
          $scope.res.estado = false;
          console.log('dis');
        }
      });
    }else{
      console.log('tres');
      i.favorito = '';
      i.estado = false;
    }
  })
  .error(function(data){
    var alertPopup=$ionicPopup.alert({
      title: 'Error',
      subTitle:'Servidor no disponible, vuelva a intentarlo.',
      templateUrl:'tab-buscar.html',
      okType:'default: button-assertive'
    });
  })
  .finally(function($ionicLoading) {
    $scope.hide();
  });

  $scope.tabla=false;

  $scope.ShowTabla= function($valor){
    $scope.tabla=!$scope.tabla;
    if ($scope.tabla) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>Cargando...',
        duration: 3000
      });
    }
  }

  $scope.showPdf = function(index) {
    $scope.activeSlide = index;
    $scope.showModal('templates/pdf.html');
  }

  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  // Close the modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };

  // Rating option

  $scope.maximumRate = 5;
  $scope.color = 'energized';

  $scope.selectedStar = function (rate,idItem) {
    console.log(rate)
    var link = 'http://data.utpl.edu.ec/wsmovil/social/crearrank';

    $http.post(link, {
      "usuario":$localStorage.usuarioID,
      "sujeto":idItem,
      "rank":rate
    }).then(function (res){
      console.log(res);
      $rootScope.agregarRanks();
    });
  }

});
