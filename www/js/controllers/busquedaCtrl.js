starter.controller('busquedaCtrl', function($scope,$state, $localStorage,srvBusqueda, $ionicActionSheet,$ionicPopup,$ionicLoading,$ionicModal,$http,$cordovaKeyboard,$ionicScrollDelegate,$rootScope, srvFavorito,$cordovaSQLite, srvTop, $ionicTabsDelegate, $cordovaToast, $ionicHistory) {
  $scope.$on('$ionicView.beforeEnter', function(){
    $rootScope.ConexionInter();
    if ($rootScope.refrescar.Refrescar) {
      switch ($rootScope.refrescar.Servicio) {
        case 'ten':
          $scope.obtenerTopTen();
          $rootScope.refrescar.Refrescar = false;
          break;
        case 'do':
          $scope.do($scope.search,'','','','','');
          $rootScope.refrescar.Refrescar = false;
          break;
        case 'doFas':
          $scope.do($scope.fasJson.principal,f1,f2,f3,f4,f5);
          $rootScope.refrescar.Refrescar = false;
          break;
        default:

      }
    }
  });

  $scope.lugar = false;
  $scope.more = false;
  $scope.btnFacetas = true;
  $scope.noResults = false;
  $rootScope.defaultImg = 'img/default_avatar2.png';

  // //SIGUIENTE TAB SWIPE
  // $scope.goForward = function () {
  //   var selected = $ionicTabsDelegate.selectedIndex();
  //   if (selected != -1) {
  //     $ionicTabsDelegate.select(selected + 1);
  //   }
  // }
  // //ANTERIOR TAB SWIPE
  // $scope.goBack = function () {
  //   var selected = $ionicTabsDelegate.selectedIndex();
  //   if (selected != -1 && selected != 0) {
  //     $ionicTabsDelegate.select(selected - 1);
  //   }
  // }

  $scope.cargarPaginador = function (parametro,f1,f2,f3,f4,f5) {
    if ($scope.page<=pagTotal) {
      $cordovaToast.show('Página: '+$scope.page, 'short', 'bottom');
      $http.get('http://data.utpl.edu.ec/biows/search/busquedainicial?cadena='+parametro+'&paginado=5&page='+$scope.page+'&f1='+f1+'&f2='+f2+'&f3='+f3+'&f4='+f4+'&f5='+f5+'')
      .success(function (data) {
        angular.forEach(data.data, function (i) {
          //AGREGAR CLAVE FAVORITO
          //para saber si ha sido marcado
          if ($localStorage.usuarioID != null) {
            var query = "SELECT * FROM favoritos WHERE id = ?";
            $cordovaSQLite.execute(db, query, [i.id]).then(function(res) {
              if(res.rows.length > 0) {
                i.favorito = 'estrellaOn';
                i.estado = true;
                console.log('uno');
              } else {
                i.favorito = '';
                i.estado = false;
                console.log('dis');
              }
            });
          }else{
            console.log('tres');
            i.favorito = '';
            i.estado = false;
          }
          $scope.res.push(i);
        });
      })

      .error (function (data) {

        // Disable infinite scroll since we've got an error.
        $scope.more = false;

        // ALERTA SERVIDOR NO DISPONIBLE
        $cordovaToast.show('Servidor no disponible', 'long', 'bottom');
        // $ionicPopup.alert({
        //   title: 'Servidor no responde.',
        //   template: 'por favor intente de nuevo.'
        // });

      })

      .finally(function () {

        // On finish, increment to next page and alert infiniteScroll to close.
        $scope.page++;
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }else{
      $scope.more = false;
    }
  };

  $scope.verFacetas = function () {
    if (!$scope.btnFacetas) {
      $ionicModal.fromTemplateUrl('templates/modalFacetas.html', {
        scope: $scope,
        animation: 'slide-in-up'
      })
      .then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    }else{
      $cordovaToast.show('No hay filtros.', 'long', 'bottom');
    }
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.fasJson = {
    'principal' : '',
    '0' : '',
    '1' : '',
    '2' : '',
    '3' : '',
    '4' : ''
  }

  $scope.busFacetas = function () {
    $scope.modal.hide();
    console.log($scope.fasJson);
    $scope.fasJson.principal = $scope.search;
    f1 = $scope.fasJson[0];
    f2 = $scope.fasJson[1];
    f3 = $scope.fasJson[2];
    f4 = $scope.fasJson[3];
    f5 = $scope.fasJson[4];
    $scope.do($scope.fasJson.principal,f1,f2,f3,f4,f5);
    $rootScope.refrescar.Servicio = 'doFas';
    //$scope.resetFaseta = false;
  }

  $scope.irDetalle= function(idRecibe){
    $state.go('tabs.detalle',{
      id:idRecibe
    });
  }

  $scope.resetFaseta = true;
//ACCION AL PRESIONAR TECLA INTRO
  $scope.do = function (search,f1,f2,f3,f4,f5) {
    $rootScope.ConexionInter();
    $scope.page = 2;
    $rootScope.refrescar.Servicio = 'do';
    console.log($rootScope.refrescar);
    //OCULTAR TECLADO
    $cordovaKeyboard.close();
    // MOSTRAR LOADING SPINNER
    $scope.show($ionicLoading);
    // SOLICITUD DE DATOS BUSQUEDA PRINCIPAL
    srvBusqueda.servicioBusqueda(search,f1,f2,f3,f4,f5)
    .success(function(data){
      $scope.res = ' ';

      //TOTAL DE RESULTADOS DE LA CONSULTA
      maxRes = data.maxresults;
      if (maxRes > 0) {
        //CALCULAR NUMERO DE PAGINAS
        pagTotal = Math.ceil(maxRes/5);
        console.log(pagTotal);
        //RESETEAR SCROOL AL INICIO
        $ionicScrollDelegate.scrollTop();
        //ALMACENAR JSON DE LA CONSULTA
        $scope.res=data.data;
        //ALMACENAR TOTAL RESULTADOS
        $scope.totRes = maxRes;
        //AGREGAR CLAVE FAVORITO
        //para saber si ha sido marcado
        angular.forEach($scope.res, function (i) {

          if ($localStorage.usuarioID != null) {
            var query = "SELECT * FROM favoritos WHERE id = ?";
            $cordovaSQLite.execute(db, query, [i.id]).then(function(res) {
              if(res.rows.length > 0) {
                i.favorito = 'estrellaOn';
                i.estado = true;
                console.log('uno');
              } else {
                i.favorito = '';
                i.estado = false;
                console.log('dis');
              }
            });
          }else{
            console.log('tres');
            i.favorito = '';
            i.estado = false;
          }
        });
        //ALMACENAR JSON DE LAS FACETAS
        $scope.facetas=data.facetas;
        //ACTIVAR PAGINADOR
        $scope.more = true;
        //ACTIVAR BOTON FACETAS
        $scope.btnFacetas = false;
        //DESACTIVAR AVISO NO RESULTADOS
        $scope.noResults = false;
        //DESACTIVAR populares
        $scope.populares = false;
      }else{
        //DESACTIVAR BOTON FACETAS
        $scope.btnFacetas = true;
        //ACTIVAR AVISO NO RESULTADOS
        $scope.noResults = true;
        //DESACTIVAR PAGINADOR
        $scope.more = false;
        //ALMACENAR JSON DE LA CONSULTA
        $scope.res="";
        //DESACTIVAR populares
        $scope.populares = false;
      }
    })
    .error(function(data){
      //POP UP DE ERROR
      // var alertPopup=$ionicPopup.alert({
      //   title: 'Error de conexión.',
      //   subTitle:'Servidor no disponible, vuelva a intentarlo.',
      //   okType:'default: button-assertive'
      // });
      $cordovaToast.show('Servidor no disponible', 'long', 'bottom');
      //DESACTIVAR EL PAGINADOR
      $scope.more = false;
      //DESACTIVAR BOTON  FACETAS
      $scope.btnFacetas = false;
    })
    .finally(function($ionicLoading) {
      //DESACTIVAR LOADING SPINNER
      $scope.hide($ionicLoading);
    });

    if (!$scope.resetFaseta) {
      $scope.fasJson = {
        'principal' : '',
        '0' : '',
        '1' : '',
        '2' : '',
        '3' : '',
        '4' : ''
      }
      $scope.resetFaseta = true;
    }
  }

  $scope.show = function() {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner>'
    });
  };

  $scope.hide = function(){
    $ionicLoading.hide();
  };



//MOSTAR TOP TEN
  $scope.obtenerTopTen = function (item) {
    $rootScope.refrescar.Servicio = 'ten';
    $scope.show();
    srvTop.servicioTopRank()
    //SI POST SE EJECUTA CORRECTAMENTE
    .success(function(data){
      $scope.res = data;
      //AGREGAR CLAVE FAVORITO
      //para saber si ha sido marcado
      angular.forEach($scope.res, function (i) {

        if ($localStorage.usuarioID != null) {
          var query = "SELECT * FROM favoritos WHERE id = ?";
          $cordovaSQLite.execute(db, query, [i.id]).then(function(res) {
            if(res.rows.length > 0) {
              i.favorito = 'estrellaOn';
              i.estado = true;
              console.log('uno');
            } else {
              i.favorito = '';
              i.estado = false;
              console.log('dis');
            }
          });
        }else{
          console.log('tres');
          i.favorito = '';
          i.estado = false;
        }
      });
      //MOSTAR SEPARADOR DE POPULARES
      $scope.populares = true;
      //RESETEAR SCROOL AL INICIO
      $ionicScrollDelegate.scrollTop();
    })
    //SI HAY UN ERRO EN EL GET
    .error(function(data){
      //POP UP DE ERROR
      $cordovaToast.show('Servidor no disponible', 'long', 'bottom');
      // var alertPopup=$ionicPopup.alert({
      //   title: 'Error de conexión.',
      //   subTitle:'Servidor no disponible, vuelva a intentarlo.',
      //   okType:'default: button-assertive'
      // });
    })
    //AL FINALIZAR EL SERVICIO
    .finally(function($ionicLoading) {
      $scope.hide();
    });
  }

//BORRAR BUSQUEDA
 $scope.borrarBusqueda = function () {
   //VACIAR CAMPO SEARCH
   $scope.search = '';
   //EJCUTAR FUNCION OBTENER TOP
   $scope.obtenerTopTen();
   //DESACTIVAR AVISO NO RESULTADOS
   $scope.noResults = false;
   //DESACTIVAR PAGINADOR
   $scope.more = false;
   //ELIMINAR TOTAL DE RESULTADOS
   $scope.totRes = undefined;
   //DESACTIVAR BOTON FACETAS
   $scope.btnFacetas = true;
 }
 //BOTON DE BORRAR BUSQUEDA
 $scope.verClear = function () {
   if ($scope.search === '' || typeof $scope.search == 'undefined') {
     return false;
   }else{
     return true;
   }
 }
});
