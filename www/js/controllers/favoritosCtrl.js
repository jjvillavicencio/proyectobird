starter.controller("favoritosCtrl", function($state, $scope, $http, $localStorage, $location,$cordovaOauthUtility, $cordovaSQLite,$rootScope) {
  $scope.$on('$ionicView.beforeEnter', function(){
    $scope.results = '';
    $rootScope.ConexionInter();
    $scope.usuarioID = $localStorage.usuarioID;
    //VERIFICAR SI ESTA REGISTRADO
    if ($localStorage.usuarioID != null) {
      // CREAR BASE DE DATOS SQLITE
      db = $cordovaSQLite.openDB( {
        name: "UTPL.birds",
        iosDatabaseLocation: 'default'
      });
      $rootScope.agregarFavoritos();
      $scope.cargar = function () {
        var query = "SELECT * FROM favoritos WHERE idUsuario = ?";
        console.log("aqui user: "+$localStorage.usuarioID);
        $cordovaSQLite.execute(db, query, [$localStorage.usuarioID]).then(function(res) {
          console.log(JSON.stringify(res));
          if(res.rows.length > 0) {
            $scope.results=[];
            for(var i=0; i<res.rows.length; i++){
              res.rows.item(i).favorito = 'estrellaOn';
              $scope.results.push(res.rows.item(i));
            }
            $scope.cantFav = true;
            console.log('Este res:');
            console.log($scope.results);

          } else {
            alert('resultado consulta menor a 0');
          }
        }, function (err) {
          // alert(JSON.stringify(err));
          $scope.cantFav = false;
        });
      }
      $scope.cargar();

    }else{
    console.log($localStorage.usuarioID);
  }
  });

  $scope.lugar = true;

  $scope.fav = function (estado) {
    if (estado) {
      return 'estrellaOn'
    }else{
      return ''
    }
  }
    $scope.irDetalle= function(idRecibe){
      $state.go('tabs.favoritosDet',{
        id:idRecibe,
        tab:'fav'
      });
    }
});
