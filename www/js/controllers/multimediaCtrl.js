starter.controller("multimediaCtrl",['$scope','$state','srvGaleria','$ionicPopup','$stateParams','$ionicModal','$sce', '$rootScope',function($scope,$state,srvGaleria,$ionicPopup,$stateParams,$ionicModal,$sce, $rootScope){
    $scope.$on('$ionicView.beforeEnter', function(){
      $rootScope.ConexionInter();
    });

    $scope.verMultimedia = function (tipo) {
      switch (tipo) {
        case 'imagenes':
            if ($stateParams.tipo == 'image') {
              return true;
            }else {
              return false;
            }
          break;
        case 'videos':
            if ($stateParams.tipo == 'video') {
              return true;
            }else {
              return false;
            }
          break;
        case 'sonidos':
            if ($stateParams.tipo == 'sound') {
              return true;
            }else {
              return false;
            }
          break;

      }
    }

    srvGaleria.servicioGaleria($stateParams.id, $stateParams.tipo).success(function(data){
      $scope.res=data.media;
    })
    .error(function(data){
      var alertPopup=$ionicPopup.alert({
        title: 'Error',
        subTitle:'Servidor no disponible, vuelva a intentarlo.',
        templateUrl:'tab-buscar.html',
        okType:'default: button-assertive'
      });
    });


  	$scope.showImages = function(index) {
  		$scope.activeSlide = index;
  		$scope.showModal('templates/image-popover.html');
  	}

  	$scope.showModal = function(templateUrl) {
  		$ionicModal.fromTemplateUrl(templateUrl, {
  			scope: $scope,
  			animation: 'slide-in-up'
  		}).then(function(modal) {
  			$scope.modal = modal;
  			$scope.modal.show();
  		});
  	}

  	// Close the modal
  	$scope.closeModal = function() {
  		$scope.modal.hide();
  		$scope.modal.remove()
  	};

    $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
    }

    $scope.clipSrc = 'http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_2mb.mp4';

    $scope.playVideo = function() {
      $scope.showModal('templates/video-popover.html');
    }

    $scope.playSonido = function (item) {
      if($scope.media){
        $scope.media.pause()
      }
      $scope.media = new Audio();
      $scope.media.src = item.image;
      $scope.media.load();
      $scope.media.play();

    }
}]);
