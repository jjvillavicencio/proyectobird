angular.module('ionRate', [])
  .directive('ionRate', [
    function() {
      'use strict';

      return {
        restrict: 'AEC',
        replace: false,
        template: '<div class="ion-rating">' +
        '<ul>' +
        '<li ng-repeat="i in getNumber(maximum) track by $index" ng-click="selectRate($index + 1)" class="{{color}}">' +
        '<i class=" {{emojis.emoticon[$index]}}" ng-if="$index + 1 <= rate "></i>' +
        '<i class=" {{emojis.emoticon[$index]}}" style="color:rgb(102, 102, 102)" ng-if="rate < $index + 1"></i>' +
        '</li>' +
        '</ul>' +
        '</div>',
        scope: {
          maximum: '=',
          rate: '=',
          color: '=',
          selectStar: '&'
        },
        controller : function($scope, $rootScope, $localStorage) {
          $scope.color = $scope.color || 'calm';
          $scope.maximum = $scope.maximum || 5;
          $scope.emojis = {
            'emoticon': [
              'flaticon1-angry-black-face',
              'flaticon1-angry-black-emoticon-face',
              'flaticon1-neutral-black-emoticon-face-symbol',
              'flaticon1-smiling-black-emoticon-face',
              'flaticon1-laughing-emoticon-black-happy-face'
            ]
          };
          $scope.rate = $scope.rate;

          $scope.getNumber = function(num) {
            return new Array(num);
          };

          $scope.selectRate = function (index) {
            if ($localStorage.usuarioID != null) {
              $scope.rate = index;
              $scope.selectStar({rating: $scope.rate})
            }else{
              //POP UP DE ERROR
              var alertPopup=$ionicPopup.alert({
                title: 'Sin acceso',
                subTitle:'Necesitas registrate para usar esta opción.',
                okType:'default: button-assertive'
              });
            }

          };

        }
      };

    }
  ]);
