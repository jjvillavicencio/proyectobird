starter.controller("loginCtrl", function($scope, $cordovaOauth, $localStorage, $http, $location,$cordovaOauthUtility, $cordovaSQLite, $rootScope, $ionicLoading, srvFavorito, $cordovaToast) {
  //CARGAR SPINNER
  $scope.show = function() {
    $ionicLoading.show({
      template: '<p>Loading...</p><ion-spinner></ion-spinner>'
    });
  };
  //OCULTAR SPINNER
  $scope.hide = function(){
    $ionicLoading.hide();
  };

  if ($rootScope.userDB != null) {
    $scope.acceso = false;
    switch ($rootScope.userDB.red_social) {
      case 'facebook':
      $scope.accesoFb = true;
      break;
      case 'twitter':
      $scope.accesoTw = true;
      break;
      case 'google':
      $scope.accesoGoo = true;
      break;
    }
  }else{
    $scope.acceso = true;
  }

  $scope.login = function($redSocial) {
    $rootScope.ConexionInter();
    switch ($redSocial) {
      case 'facebook':
        $cordovaOauth.facebook("1543514235941585", ["email","public_profile", "user_location", "user_relationships"])
        .then(function(result) {
          $localStorage.accessToken = result.access_token;
          $scope.init('facebook');
          $location.path("/tab/usuario");
        }, function(error) {
          alert("Error de acceso con Facebook, intente de nuevo.");
          console.log(error);
          $scope.acceso = true;
          $scope.accesoFb = false;
        });
        break;
      case 'google':
        $cordovaOauth.google("604695898786-p5onq9h2f9i6oegr7g1is9hq69h4aet6.apps.googleusercontent.com", ["email","profile", "openid"])
        .then(function(result) {
          $localStorage.accessToken = result.access_token;
          $scope.init('google');
          $location.path("/tab/usuario");
        }, function(error) {
          alert("Error de acceso con Google+, intente de nuevo.");
          console.log(error);
          $scope.acceso = true;
          $scope.accesoGoo = false;
        });
        break;
      case 'twitter':
        $cordovaOauth.twitter("IEZAfV2x4AHNJ3GrNJzPOjKpW", "2MkN18bVIjEhnapOaCrczAcvjzpbpFwStrcc4FRVHSD2EChL3h")
        .then(function(result) {
          $localStorage.accessToken = result;
          $scope.init('twitter');
          $location.path("/tab/usuario");
        }, function(error) {
          alert("Error de acceso con Twitter, intente de nuevo.");
          $scope.acceso = true;
          $scope.accesoTw = false;
          console.log(error);
        });
        break;
    }
  };

  $scope.init = function($perfilSocial) {
    switch ($perfilSocial) {
      case 'facebook':
        if($localStorage.hasOwnProperty("accessToken") === true) {
          // SPINNER EN POST DE USUARIO
          $scope.show($ionicLoading);
          // OBTENER DATOS CON TOKEN DE FACEBOOK
          $http.get("https://graph.facebook.com/v2.2/me", {
            params: {
              access_token: $localStorage.accessToken,
              fields: "email,id,name,gender,location,website,picture,relationship_status",
              format: "json"
            }
          })
          .then(function(result) {
            // ALMACENAR DATOS DE FACEBOOK DEL USUARIO EN VARIABLE
            $scope.profileData = result.data;
            var link = 'http://data.utpl.edu.ec/wsmovil/social/crearusuario';
            // INSERTAR DATOS DE USUARIO EN BD EXTERNA
            $http.post( link, {
              "user":result.data.name,
              "nombre":result.data.name,
              "idUserRedSocial":result.data.id,
              "redSocial":"facebook"
            })
            .then(function (res){
              // CREAR TABLA USUARIO EN SQLite
              $cordovaSQLite.execute( db, "CREATE TABLE IF NOT EXISTS usuario (id integer, user text, nombre text, id_user_red_social text, red_social text, url_img text,genero text, email text, location text)" );

              var query = "INSERT INTO usuario (id, user, nombre, id_user_red_social, red_social, url_img, genero, email, location) VALUES (?,?,?,?,?,?,?,?,?)";
              // INSERTAR DATOS DEL USUARIO EN BD INTERNA
              $cordovaSQLite.execute( db, query, [ res.data, result.data.name, result.data.name, result.data.id, 'facebook', result.data.picture.data.url, result.data.gender, result.data.email, result.data.location.name ] )
              .then(function(result2) {
                console.log("INSERT ID -> " + res.data);
                // alert("INSERT ID -> " + res.data);
                $cordovaToast.show('Acceso correcto', 'long', 'bottom');
                $rootScope.userId = res.data;
                $scope.acceso = false;
                $scope.accesoFb = true;
                $rootScope.agregarFavoritos();
                $rootScope.agregarRanks();
              }, function (err) {
                console.error("Error al insertar datos SQLite: "+err);
                alert("Error al insertar datos SQLite: "+err);
              });

              $scope.hide($ionicLoading);
            },function(error) {
              console.log(error);
              alert("Error al hacer POST"+res.data);
            });
          }, function(error) {
            alert("Habido un error al obtener tus datos de Facebook, intenta de nuevo");
            console.log(error);
          });
        } else {
          // No existe el token de verificacion facebook
          alert("No has iniciado sesiòn");
          $location.path("/tab/usuarios");
        }
        break;
      case 'google':
        if($localStorage.hasOwnProperty("accessToken") === true) {
          $scope.show($ionicLoading);
          $http.get("https://www.googleapis.com/oauth2/v1/userinfo", {
            params: {
              access_token: $localStorage.accessToken,
              fields: "name,id,given_name,picture,gender,email,locale",
              format: "json"
            }
          })
          .then(function(result) {
            $scope.profileData = result.data;
            var link = 'http://data.utpl.edu.ec/wsmovil/social/crearusuario';
            // INSERTAR DATOS DE USUARIO EN BD EXTERNA
            $http.post( link, {
              "user":result.data.given_name,
              "nombre":result.data.name,
              "idUserRedSocial":result.data.id,
              "redSocial":"google"
            })
            .then(function (res){
              console.log(res);
              // CREAR TABLA USUARIO EN SQLite
              $cordovaSQLite.execute( db, "CREATE TABLE IF NOT EXISTS usuario (id integer, user text, nombre text, id_user_red_social text, red_social text, url_img text, email text, genero text, idioma)" );
              var query = "INSERT INTO usuario (id, user, nombre, id_user_red_social, red_social, url_img, email, genero, idioma) VALUES (?,?,?,?,?,?,?,?,?)";
              // INSERTAR DATOS DEL USUARIO EN BD INTERNA
              $cordovaSQLite.execute( db, query, [ res.data, result.data.given_name, result.data.name, result.data.id, 'google', result.data.picture, result.data.email, result.data.gender, result.data.locale ] )
              .then(function(result2) {
                console.log("INSERT ID -> " + res.insertId);
                $cordovaToast.show('Acceso correcto', 'long', 'bottom');
                $rootScope.userId = res.data;
                $scope.acceso = false;
                $scope.accesoGoo = true;
                $rootScope.agregarFavoritos();
                $rootScope.agregarRanks();
              }, function (err) {
                console.log("Error al insertar datos SQLite: "+err);
                alert("Error al insertar datos SQLite: "+err);
              });
            },function(error) {
              console.log(error);
              alert("Error al hacer POST"+res.data);
            });
            $scope.hide($ionicLoading);
          }, function(error) {
            alert("There was a problem getting your profile.  Check the logs for details.");
            $scope.profileData = error;
            $scope.hide($ionicLoading);
            console.log(error);
          });
        } else {
          alert("Not signed in");
          $location.path("/tab/usuarios");
        }
        break;
      case 'twitter':
        if($localStorage.hasOwnProperty("accessToken") === true) {
          var api_key = "IEZAfV2x4AHNJ3GrNJzPOjKpW";
          var api_secret = "2MkN18bVIjEhnapOaCrczAcvjzpbpFwStrcc4FRVHSD2EChL3h";
          var oauth_token = null;
          var oauth_token_secret = null;
          var user_id = null;
          var screen_name = null;
          var url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
          var method = "GET";

          oauth_token = $localStorage.accessToken.oauth_token;
          oauth_token_secret = $localStorage.accessToken.oauth_token_secret;
          user_id = $localStorage.accessToken.user_id;
          screen_name = $localStorage.accessToken.screen_name;
          var oauthObject = {
            oauth_consumer_key: api_key,
            oauth_nonce: $cordovaOauthUtility.createNonce(32),
            oauth_signature_method: "HMAC-SHA1",
            oauth_token: oauth_token,
            oauth_timestamp: Math.round((new Date()).getTime() / 1000.0),
            oauth_version: "1.0",
          };

          var signatureObj = $cordovaOauthUtility.createSignature(method, url, oauthObject,{count:1}, api_secret, oauth_token_secret);
          var config = {
            headers:  {
              'Authorization': signatureObj.authorization_header
            }
          };

          $scope.show($ionicLoading);
          $http.get(url + "?count=1", config)
          .success(function(response) {
            $scope.profileData = response[0].user;
            var link = 'http://data.utpl.edu.ec/wsmovil/social/crearusuario';
            // INSERTAR DATOS DE USUARIO EN BD EXTERNA
            $http.post( link, {
              "user":response[0].user.name,
              "nombre":response[0].user.screen_name,
              "idUserRedSocial":response[0].user.id,
              "redSocial":"twitter"
            })
            .then(function (res){
              console.log(res);
              // CREAR TABLA USUARIO EN SQLite
              $cordovaSQLite.execute( db, "CREATE TABLE IF NOT EXISTS usuario (id integer, user text, nombre text, id_user_red_social text, red_social text, url_img text, banner text, description text, location)" );

              var query = "INSERT INTO usuario (id, user, nombre, id_user_red_social, red_social, url_img, banner, description, location) VALUES (?,?,?,?,?,?,?,?,?)";
              // INSERTAR DATOS DEL USUARIO EN BD INTERNA
              $cordovaSQLite.execute( db, query, [
                res.data,
                response[0].user.name,
                response[0].user.screen_name,
                response[0].user.id,
                'twitter',
                response[0].user.profile_image_url,
                response[0].user.profile_banner_url,
                response[0].user.description,
                response[0].user.location

              ])
              .then(function(result2) {
                console.log("INSERT ID -> " + result2.insertId);
                $cordovaToast.show('Acceso correcto', 'long', 'bottom');
                $rootScope.userId = res.data;
                $scope.acceso = false;
                $scope.accesoTw = true;
                $rootScope.agregarFavoritos();
                $rootScope.agregarRanks();
              }, function (err) {
                console.log("Error al insertar datos SQLite: "+err);
                alert("Error al insertar datos SQLite: "+err);
              });
              $scope.hide($ionicLoading);
            },function(error) {
              console.log(error);
              alert("Error al hacer POST"+res.data);
            });
          })
          .error(function(error){
            $scope.show($ionicLoading);
            alert("Error: " + error);
          });
        }
    }
  };

  $scope.cerrarSesion = function () {
    $cordovaSQLite.deleteDB({
      name: 'UTPL.birds',
      location: 'default'
    });
    $scope.acceso = true;
    $scope.accesoFb = false;
    $scope.accesoGoo = false;
    $scope.accesoTw = false;
    $rootScope.userId = null;
  }

  $scope.postPrueba = function () {

    var link = 'http://data.utpl.edu.ec/wsmovil/social/crearusuario';
    $http.post(link, {
      "user":"Jairo Vs",
      "nombre":"Jairo Vs",
      "idUserRedSocial":"1664752973797300",
      "redSocial":"facebook"
    },{timeout:"4000"})
    .then(function (res){
      console.log(res);
      alert(res.data);
    },function(error) {
      console.log('E: '+ JSON.stringify(error));
      alert('aqui');
    })
  }


});
